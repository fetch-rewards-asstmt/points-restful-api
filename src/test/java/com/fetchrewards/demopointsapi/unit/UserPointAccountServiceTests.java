package com.fetchrewards.demopointsapi.unit;

import com.fetchrewards.demopointsapi.domain.UserPointAccount;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import com.fetchrewards.demopointsapi.exceptions.NotEnoughPointsToDeductException;
import com.fetchrewards.demopointsapi.repo.NewPoints;
import com.fetchrewards.demopointsapi.repo.UserPointAccountRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ModelMapper.class})
class UserPointAccountServiceTests {


    @MockBean
    private UserPointAccountRepo repo;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    void shouldAddPoints() {
        Mockito.when(repo.getById(Mockito.anyLong())).thenReturn(initNewAccount());

        repo.getById(Mockito.anyLong()).addPoints(new NewPointsDto("Axis", 100, LocalDateTime.now().minusDays(3)));
        Mockito.verify(repo).saveNewPoints(Mockito.anyLong(), Mockito.any(NewPoints.class));
        Mockito.verify(repo, Mockito.times(1)).getTotalPayerPoints(Mockito.anyLong(), Mockito.anyString());
        Mockito.verify(repo, Mockito.times(1)).updatePayerTotalPoints(Mockito.anyLong(), Mockito.anyString(), Mockito.anyLong());
    }

    @Test
    void shouldDeductPoints() {

        Mockito.when(repo.getTotalUserPoints(Mockito.anyLong())).thenReturn(200L);
        Mockito.when(repo.getNewPoints(Mockito.anyLong())).thenReturn(initNewPoints());
        Mockito.when(repo.getById(Mockito.anyLong())).thenReturn(initNewAccount());

        List<TransactionDto> transactionDtos = repo.getById(Mockito.anyLong()).deductPoints(100);
        Mockito.verify(repo, Mockito.times(1)).getTotalUserPoints(Mockito.anyLong());
        Mockito.verify(repo, Mockito.times(1)).getNewPoints(Mockito.anyLong());

        String expectedNameMoa = "Moa";
        String expectedNameRivers = "Rivers";

        Map<String, Long> transactionMap = transactionDtos.stream().collect(Collectors.toMap(TransactionDto::getPayerName, TransactionDto::getPoints));
        Assertions.assertEquals(-30, transactionMap.getOrDefault(expectedNameMoa, 0L));
        Assertions.assertEquals(-70, transactionMap.getOrDefault(expectedNameRivers, 0L));
    }

    private PriorityQueue<NewPoints> initNewPoints() {
        return new PriorityQueue<>(Arrays.asList(
                new NewPoints("Moa", 50, LocalDateTime.now().minusDays(2), LocalDateTime.now())
                , new NewPoints("Moa", 20, LocalDateTime.now().minusDays(3), LocalDateTime.now())
                , new NewPoints("Rivers", 70, LocalDateTime.now().minusDays(4), LocalDateTime.now())
        ));
    }

    @Test
    void shouldThrowNotEnoughDeductPoints() {
        Mockito.when(repo.getTotalUserPoints(Mockito.anyLong())).thenReturn(0L);
        Mockito.when(repo.getById(Mockito.anyLong())).thenReturn(initNewAccount());
        Assertions.assertThrows(NotEnoughPointsToDeductException.class, () -> {
            repo.getById(Mockito.anyLong()).deductPoints(100);//NOSONAR
            Mockito.verify(repo, Mockito.times(1)).getTotalUserPoints(Mockito.anyLong());//NOSONAR
        });
    }

    private UserPointAccount initNewAccount() {
        return new UserPointAccount(1, modelMapper, repo);
    }

}
