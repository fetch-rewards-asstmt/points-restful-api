package com.fetchrewards.demopointsapi.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fetchrewards.demopointsapi.controller.PointAccountController;
import com.fetchrewards.demopointsapi.domain.UserPointAccount;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.repo.UserPointAccountRepo;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;

@WebMvcTest(controllers = {PointAccountController.class, ModelMapper.class})
class PointAccountControllerTests {

    private static final String BASE_PATH = "/v1/points";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserPointAccountRepo repoMock;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private UserPointAccount userPointAccountMock;

    @Test
    void shouldGetPointBalance() throws Exception {
        Mockito.when(repoMock.getById(Mockito.anyLong())).thenReturn(userPointAccountMock);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "/balance/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void shouldDeductPoints() throws Exception {
        Mockito.when(repoMock.getById(Mockito.anyLong())).thenReturn(userPointAccountMock);

        mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "100"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    void should400_Negative_Points_DeductPoints() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "-100"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void shouldAddPoints() throws Exception {
        Mockito.when(repoMock.getOrCreateUserPointAccount(Mockito.anyLong())).thenReturn(userPointAccountMock);
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(initNewPointsDto())))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    private NewPointsDto initNewPointsDto() {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("DONNAN");
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(2));
        return newPoints;
    }

    @Test
    void should400_Missing_Payer_Name_AddPoints() throws Exception {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(2));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints))).andDo(MockMvcResultHandlers.log()).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should400_Transaction_Date_In_Future_AddPoints() throws Exception {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("DONNAN");
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(-2));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints))).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }



}
