package com.fetchrewards.demopointsapi.unit;

public class EmileComparator implements IntegerComparator<Integer>, StringComparator<String>, ArrayComparator<Integer> {

    @Override
    public boolean compare(Integer a, Integer b) {
        return a.compareTo(b) == 0;
    }

    @Override
    public boolean compare(String a, String b) {
        return a.compareTo(b) == 0;
    }

    @Override
    public boolean compare(Integer[] a, Integer[] b) {
        if(a.length != b.length) return false;
        int i = 0;
        while (i < a.length){
            if(!a[i].equals(b[i])) return false;
            i++;
        }
        return true;
    }
}

interface IntegerComparator<T extends Integer>{
    boolean compare(T a, T b);
}

interface StringComparator<T extends String>{
    boolean compare(T a, T b);
}

interface ArrayComparator<T extends Integer>{
    boolean compare(T[] a, T[] b);
}


