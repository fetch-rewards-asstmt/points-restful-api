package com.fetchrewards.demopointsapi.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fetchrewards.demopointsapi.dto.BalanceDto;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import com.fetchrewards.demopointsapi.repo.UserPointAccountRepo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
@AutoConfigureMockMvc
class PointAccountRestfulServiceTests {

    private static final String BASE_PATH = "/v1/points";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserPointAccountRepo repo;


    @AfterEach
    public void clearTestData() {
        repo.clear();
    }


    @Test
    void shouldDeductPoints_with_Assessment_Data() throws Exception {
        initNewPointsAssessmentData().forEach(this::seedTestData);

        MvcResult deductPointResult = mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "5000"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        TransactionDto[] deductPointTranasctionDtos = objectMapper.readerForArrayOf(TransactionDto.class).readValue(deductPointResult.getResponse().getContentAsString());
        String expectedNameDannon = "DANNON";
        String expectedNameMillerCoors = "MILLER COORS";
        String expectedNameUnilever = "UNILEVER";

        Map<String, Long> transactionMap = Arrays.stream(deductPointTranasctionDtos).collect(Collectors.toMap(TransactionDto::getPayerName, TransactionDto::getPoints));
        Assertions.assertEquals(-100, transactionMap.getOrDefault(expectedNameDannon, 0L));
        Assertions.assertEquals(-4700, transactionMap.getOrDefault(expectedNameMillerCoors, 0L));
        Assertions.assertEquals(-200, transactionMap.getOrDefault(expectedNameUnilever, 0L));

        //Verify balance
        MvcResult balanceResult = mockMvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "/balance/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();


        BalanceDto[] balanceDtos = objectMapper.readerForArrayOf(BalanceDto.class).readValue(balanceResult.getResponse().getContentAsString());
        Map<String, Long> balanceMap = Arrays.stream(balanceDtos).collect(Collectors.toMap(BalanceDto::getPayerName, BalanceDto::getPoints));

        Assertions.assertEquals(1000, balanceMap.getOrDefault(expectedNameDannon, 0L));
        Assertions.assertEquals(5300, balanceMap.getOrDefault(expectedNameMillerCoors, 0L));
        Assertions.assertEquals(0, balanceMap.getOrDefault(expectedNameUnilever, 0L));

    }


    @Test
    void shouldDeductPoints_with_Assessment_Data_with_deduct_300_points() throws Exception {
        initNewPointsAssessmentDataWithT300H().forEach(this::seedTestData);

        MvcResult deductPointResult = mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "300"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        TransactionDto[] deductPointTranasctionDtos = objectMapper.readerForArrayOf(TransactionDto.class).readValue(deductPointResult.getResponse().getContentAsString());
        String expectedNameDannon = "DANNON";
        String expectedNameMillerCoors = "MILLER COORS";
        String expectedNameUnilever = "UNILEVER";

        Map<String, Long> transactionMap = Arrays.stream(deductPointTranasctionDtos).collect(Collectors.toMap(TransactionDto::getPayerName, TransactionDto::getPoints));
        Assertions.assertEquals(-100, transactionMap.getOrDefault(expectedNameDannon, 0L));
        Assertions.assertEquals(-200, transactionMap.getOrDefault(expectedNameUnilever, 0L));

        //Verify balance
        MvcResult balanceResult = mockMvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "/balance/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        BalanceDto[] balanceDtos = objectMapper.readerForArrayOf(BalanceDto.class).readValue(balanceResult.getResponse().getContentAsString());

        Map<String, Long> balanceMap = Arrays.stream(balanceDtos).collect(Collectors.toMap(BalanceDto::getPayerName, BalanceDto::getPoints));
        Assertions.assertEquals(1000, balanceMap.getOrDefault(expectedNameDannon, 0L));
        Assertions.assertEquals(10000, balanceMap.getOrDefault(expectedNameMillerCoors, 0L));
        Assertions.assertEquals(0, balanceMap.getOrDefault(expectedNameUnilever, 0L));

    }


    private List<NewPointsDto> initNewPointsAssessmentData() {
        /*
        add [DANNON, 300 points, 10/31 10AM] to user
        add [UNILEVER, 200 points, 10/31 11AM] to user
        add [DANNON, -200 points, 10/31 3PM] to user
        add [MILLER COORS, 10,000 points , 11/1 2PM] to user
        add [DANNON, 1000 points 11/2 2PM] to user
         */
        return new ArrayList<>(Arrays.asList(
                new NewPointsDto("DANNON", 300, LocalDateTime.of(2020, Month.OCTOBER, 31, 10, 0, 0))
                , new NewPointsDto("UNILEVER", 200, LocalDateTime.of(2020, Month.OCTOBER, 31, 11, 0, 0))
                , new NewPointsDto("DANNON", -200, LocalDateTime.of(2020, Month.OCTOBER, 31, 15, 0, 0))
                , new NewPointsDto("MILLER COORS", 10000, LocalDateTime.of(2020, Month.NOVEMBER, 1, 14, 0, 0))
                , new NewPointsDto("DANNON", 1000, LocalDateTime.of(2020, Month.NOVEMBER, 2, 14, 0, 0))
        ));
    }

    private List<NewPointsDto> initNewPointsAssessmentDataWithT300H() {
        /*
        add [DANNON, 300 points, 10/31 10AM] to user
        add [UNILEVER, 200 points, 10/31 11AM] to user
        add [DANNON, -200 points, 10/31 3PM] to user
        add [MILLER COORS, 10,000 points , 11/1 2PM] to user
        add [DANNON, 1000 points 11/2 2PM] to user
         */
        return new ArrayList<>(Arrays.asList(
                new NewPointsDto("DANNON", 300, LocalDateTime.of(2020, Month.OCTOBER, 31, 10, 0, 0))
                , new NewPointsDto("UNILEVER", 200, LocalDateTime.of(2020, Month.OCTOBER, 31, 11, 0, 0))
                , new NewPointsDto("DANNON", -200, LocalDateTime.of(2020, Month.OCTOBER, 31, 15, 0, 0))
                , new NewPointsDto("MILLER COORS", 10000, LocalDateTime.of(2020, Month.NOVEMBER, 1, 14, 0, 0))
                , new NewPointsDto("DANNON", 1000, LocalDateTime.of(2020, Month.NOVEMBER, 2, 14, 0, 0))
        ));
    }

    void seedTestData(NewPointsDto newPoints) {
        try {
            mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints))
                    .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
