package com.fetchrewards.demopointsapi.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest
@AutoConfigureMockMvc
class PointAccountRestfulServiceCustomDataTests {

    private static final String BASE_PATH = "/v1/points";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void shouldGetPointBalance() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "/balance/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void shouldDeductPoints() throws Exception {
        initNewPoints().forEach(this::seedTestData);

        MvcResult results = mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "100"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        TransactionDto[] transactionDtos = objectMapper.readerForArrayOf(TransactionDto.class).readValue(results.getResponse().getContentAsString());
        String expectedNameMoa = "Moa";
        String expectedNameRivers = "Rivers";

        Map<String, Long> transactionMap = Arrays.stream(transactionDtos).collect(Collectors.toMap(TransactionDto::getPayerName, TransactionDto::getPoints));
        Assertions.assertEquals(-30, transactionMap.getOrDefault(expectedNameMoa, 0L));
        Assertions.assertEquals(-70, transactionMap.getOrDefault(expectedNameRivers, 0L));
    }





    @Test
    void should400_Negative_Points_DeductPoints() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put(BASE_PATH + "/deduct/{userId}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("points", "-100"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void shouldAddPoints() throws Exception {

        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("DONNON");
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(1));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @Test
    void should400_AddPoints_And_Sending_Zero_Points() throws Exception {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("DONNON");
        newPoints.setPoints(0);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(1));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should400_AddPoints_And_Sending_Negative_Points_without_existing_payer() throws Exception {
        seedTestData(initPointsWithNowDate());

        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("Amazon");
        newPoints.setPoints(-200);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(1));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    private NewPointsDto initPointsWithNowDate() {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("Now Shop");
        newPoints.setPoints(200);
        newPoints.setTransactionDate(LocalDateTime.now());
        return newPoints;
    }


    @Test
    void should400_Missing_Payer_Name_AddPoints() throws Exception {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(2));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints))).andDo(MockMvcResultHandlers.log()).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should400_Transaction_Date_In_Future_AddPoints() throws Exception {
        NewPointsDto newPoints = new NewPointsDto();
        newPoints.setPayerName("DONNON");
        newPoints.setPoints(200L);
        newPoints.setTransactionDate(LocalDateTime.now().minusDays(-2));
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints))).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    private List<NewPointsDto> initNewPoints() {
        return new ArrayList<>(Arrays.asList(
                new NewPointsDto("Moa", 50, LocalDateTime.now().minusDays(2))
                , new NewPointsDto("Moa", 20, LocalDateTime.now().minusDays(3))
                , new NewPointsDto("Rivers", 70, LocalDateTime.now().minusDays(4))
        ));
    }

    private List<NewPointsDto> initNewPointsForNegaiveAdd() {
        return new ArrayList<>(Collections.singletonList(
                new NewPointsDto("Kori", 200, LocalDateTime.now().minusDays(2))
        ));
    }

    void seedTestData(NewPointsDto newPoints) {
        try {
            mockMvc.perform(MockMvcRequestBuilders.post(BASE_PATH + "/add/{userId}", 1).contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(newPoints)))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
