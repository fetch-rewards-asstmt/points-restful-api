package com.fetchrewards.demopointsapi.repo;

import com.fetchrewards.demopointsapi.domain.UserPointAccount;
import com.fetchrewards.demopointsapi.dto.TransactionDto;

import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public interface UserPointAccountRepo {
    void saveNewPoints(long userId, NewPoints newPoints);
    long getTotalPayerPoints(long userId, String payerName);
    void updatePayerTotalPoints(long userId, String payerName, long newPayerPoints);
    long getTotalUserPoints(long userId);
    PriorityQueue<NewPoints> getNewPoints(long userId);
    void saveDeductedPointsTransactions(long userId, List<TransactionDto> deductedPointsTransactions);
    Map<String, Long> getUserPayerPoints(long userId);
    PriorityQueue<NewPoints> getPayerNewPoints(Long userId, String payerName);
    UserPointAccount getById(long userId);
    UserPointAccount getOrCreateUserPointAccount(long userId);
    void clear();
}
