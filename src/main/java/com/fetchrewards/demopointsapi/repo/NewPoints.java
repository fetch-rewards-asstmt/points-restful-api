package com.fetchrewards.demopointsapi.repo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class NewPoints implements Comparable< NewPoints> {
    private String payerName;
    private long points;
    private long deductedPoints;
    private long balance;
    private LocalDateTime transactionDate;
    private LocalDateTime creationDate;

    public NewPoints(@NotBlank String payerName, long points, LocalDateTime transactionDate, LocalDateTime creationDate) {
        this.payerName = payerName;
        this.points = points;
        this.transactionDate = transactionDate;
        this.creationDate = creationDate;
    }


    public long getPointBalance(){
        return points - deductedPoints;
    }

    @Override
    public int compareTo(NewPoints o) {
        return transactionDate.compareTo(o.getTransactionDate());
    }
}
