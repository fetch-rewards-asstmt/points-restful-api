package com.fetchrewards.demopointsapi.repo;

import com.fetchrewards.demopointsapi.domain.UserPointAccount;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import com.fetchrewards.demopointsapi.exceptions.NoUserIdException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
public class InMemUserPointAccountRepo implements UserPointAccountRepo {
    private static Map<Long, UserPointAccount> store = new ConcurrentHashMap<>();
    private final ModelMapper modelMapper;

    public InMemUserPointAccountRepo(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public void saveNewPoints(long userId, NewPoints newPoints) {
        checkUserExist(userId);
        UserPointAccount userPointAccount = store.get(userId);
        store.put(userId, userPointAccount);
        userPointAccount.getOldestFirstNewPoints().add(newPoints);
    }

    @Override
    public long getTotalPayerPoints(long userId, String payerName) {
        checkUserExist(userId);
        return store.get(userId).getPayers().getOrDefault(payerName, 0L);
    }

    @Override
    public void updatePayerTotalPoints(long userId, String payerName, long newPayerPoints) {
        checkUserExist(userId);
        store.get(userId).getPayers().put(payerName, newPayerPoints);
    }

    @Override
    public long getTotalUserPoints(long userId) {
        checkUserExist(userId);
        return store.get(userId).getPayers().values().stream().reduce(0L, Long::sum);
    }

    @Override
    public PriorityQueue<NewPoints> getNewPoints(long userId) {
        checkUserExist(userId);
        return store.get(userId).getOldestFirstNewPoints();
    }

    @Override
    public void saveDeductedPointsTransactions(long userId, List<TransactionDto> deductedPointsTransactions) {
        checkUserExist(userId);
        store.get(userId).getDeductedPointsTransactions().addAll(deductedPointsTransactions);
    }

    @Override
    public Map<String, Long> getUserPayerPoints(long userId) {
        checkUserExist(userId);
        return store.get(userId).getPayers();
    }

    @Override
    public PriorityQueue<NewPoints> getPayerNewPoints(Long userId, String payerName) {
        return getNewPoints(userId).stream().filter(p -> payerName.equals(p.getPayerName())).collect(Collectors.toCollection(PriorityQueue::new));
    }

    @Override
    public UserPointAccount getById(long userId) {
        checkUserExist(userId);
        return store.get(userId);
    }


    @Override
    public UserPointAccount getOrCreateUserPointAccount(long userId) {
        UserPointAccount userPointAccount = store.getOrDefault(userId, new UserPointAccount( userId, modelMapper, this));
        store.put(userId, userPointAccount);
        return userPointAccount;
    }

    @Override
    public void clear() {
        store.clear();
    }

    private void checkUserExist(long userId) {
        if(!store.containsKey(userId)) throw new NoUserIdException(userId);
    }


}
