/**
 *
 */
package com.fetchrewards.demopointsapi.controller;

import com.fetchrewards.demopointsapi.dto.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RequestMapping( path = "/v1/error")
@RestController
public class PointAccountErrorController implements ErrorController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ErrorAttributes errorAttributes;

    public PointAccountErrorController(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
	}


    @GetMapping()
    public ResponseEntity<ErrorDto> error( @ApiIgnore WebRequest webRequest, @ApiIgnore HttpServletRequest request, @ApiIgnore HttpServletResponse response) {
        HttpStatus httpStatus = HttpStatus.valueOf(response.getStatus());
        Map<String, Object> errorData = errorAttributes.getErrorAttributes(webRequest, ErrorAttributeOptions.of(ErrorAttributeOptions.Include.values()));

        String msg = errorData.getOrDefault("message", "General error occured.").toString();
        logger.error("Request error: {}", errorData);

        if(httpStatus == HttpStatus.NO_CONTENT){
			return new ResponseEntity<>(httpStatus);
		}
        return new ResponseEntity<>(new ErrorDto(msg), httpStatus);
    }

    @Override
    public String getErrorPath() {
        return "/v1/error";
    }


}
