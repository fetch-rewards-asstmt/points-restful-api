package com.fetchrewards.demopointsapi.controller;

import com.fetchrewards.demopointsapi.dto.BalanceDto;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import com.fetchrewards.demopointsapi.repo.UserPointAccountRepo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping(path = "/v1/points", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PointAccountController {

    private final UserPointAccountRepo repo;


    public PointAccountController(UserPointAccountRepo repo) {
        this.repo = repo;
    }

    @PostMapping( path = {"/add/{userId}"})
    public ResponseEntity<TransactionDto> addPoints(@PathVariable long userId, @RequestBody @Valid NewPointsDto newPointsDto) {
        return new ResponseEntity<>(repo.getOrCreateUserPointAccount(userId).addPoints(newPointsDto), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping( path = {"/deduct/{userId}"}, params = {"points"})
    public List<TransactionDto> deductPoints(@PathVariable long userId, @RequestParam @Min(1) long points) {
        return repo.getById(userId).deductPoints(points);
    }

    @GetMapping(path = {"/balance/{userId}"})
    public List<BalanceDto> getPointBalance(@PathVariable long userId){
        return repo.getById(userId).getPointBalance();
    }
}
