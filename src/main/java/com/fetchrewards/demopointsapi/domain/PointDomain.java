package com.fetchrewards.demopointsapi.domain;

import com.fetchrewards.demopointsapi.dto.BalanceDto;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;

import java.util.List;

public interface PointDomain {
    TransactionDto addPoints(NewPointsDto newPointsDto);
    List<TransactionDto> deductPoints(long points);
    List<BalanceDto> getPointBalance();
}
