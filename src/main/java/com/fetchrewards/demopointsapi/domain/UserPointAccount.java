package com.fetchrewards.demopointsapi.domain;

import com.fetchrewards.demopointsapi.dto.BalanceDto;
import com.fetchrewards.demopointsapi.dto.NewPointsDto;
import com.fetchrewards.demopointsapi.dto.TransactionDto;
import com.fetchrewards.demopointsapi.exceptions.NoTxRecordException;
import com.fetchrewards.demopointsapi.exceptions.NotEnoughPointsToDeductException;
import com.fetchrewards.demopointsapi.repo.NewPoints;
import com.fetchrewards.demopointsapi.repo.UserPointAccountRepo;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.LongFunction;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;

public class UserPointAccount implements PointDomain {
    private final Map<String, Long> payers;
    private final PriorityQueue<NewPoints> oldestFirstNewPoints;
    private final LinkedList<TransactionDto> deductedPointsTransactions;

    private final long userId;
    private final ModelMapper modelMapper;
    private final UserPointAccountRepo repo;

    public UserPointAccount(long userId, ModelMapper modelMapper, UserPointAccountRepo repo) {
        this.userId = userId;
        this.modelMapper = modelMapper;
        this.repo = repo;
        this.payers = new ConcurrentHashMap<>();
        this.oldestFirstNewPoints = new PriorityQueue<>();
        this.deductedPointsTransactions = new LinkedList<>();
    }

    public Map<String, Long> getPayers() {
        return payers;
    }

    public PriorityQueue<NewPoints> getOldestFirstNewPoints() {
        return oldestFirstNewPoints;
    }

    public List<TransactionDto> getDeductedPointsTransactions() {
        return deductedPointsTransactions;
    }

    @Override
    public synchronized TransactionDto addPoints(NewPointsDto newPointsDto) {

        if(newPointsDto.getPoints() > 0) {
            NewPoints newPoints = modelMapper.map(newPointsDto, NewPoints.class);
            newPoints.setCreationDate(LocalDateTime.now());
            repo.saveNewPoints(userId, newPoints);

            String payerName = newPoints.getPayerName();
            creditPayerAccount(userId, payerName, newPoints.getPoints());

            return modelMapper.map(newPoints, TransactionDto.class);
        }

        return deductPointsFromSinglePayer(userId, -newPointsDto.getPoints(), newPointsDto.getPayerName() );

    }

    @Override
    public synchronized List<TransactionDto> deductPoints(long points) {
        return deductPoints(userId, points, repo::getNewPoints, repo::getTotalUserPoints);
    }

    @Override
    public List<BalanceDto> getPointBalance() {
        return repo.getUserPayerPoints(userId).entrySet().stream().map(e->new BalanceDto(e.getKey(), e.getValue())).collect(Collectors.toList());
    }



    private TransactionDto deductPointsFromSinglePayer(long userId, long points, String payerName) {
        List<TransactionDto> transactionDtos = deductPoints(userId, points, uId-> repo.getPayerNewPoints(uId, payerName), uId->repo.getTotalPayerPoints(userId, payerName));
        if(transactionDtos.isEmpty()) throw new NoTxRecordException("Something went wrong... Failed to record any transaction.");
        return transactionDtos.get(0);
    }





    private List<TransactionDto> deductPoints(long userId, long points, LongFunction<PriorityQueue<NewPoints>> oldestFirstNewPointsFunction, LongUnaryOperator totalPointFunction ) {

        long userTotalPoints = totalPointFunction.applyAsLong(userId);
        if (userTotalPoints < points) throw new NotEnoughPointsToDeductException();

        Map<String, Long> payerDeductedPoints = new HashMap<>();
        PriorityQueue<NewPoints> userOldestFirstNewPoints = oldestFirstNewPointsFunction.apply(userId);

        while (points > 0 && !userOldestFirstNewPoints.isEmpty()) {
            long pointsToDeduct;
            NewPoints oldestNewPoints = userOldestFirstNewPoints.peek();
            long balance = oldestNewPoints.getPointBalance();

            long diff = points - balance;
            pointsToDeduct = (diff > 0) ? balance : points;

            oldestNewPoints.setDeductedPoints(oldestNewPoints.getDeductedPoints() + pointsToDeduct);
            if (oldestNewPoints.getPointBalance() == 0) {
                userOldestFirstNewPoints.poll();
            }

            String payerName = oldestNewPoints.getPayerName();
            debitPayerAccount(userId, payerName, pointsToDeduct);
            payerDeductedPoints.put(payerName, payerDeductedPoints.getOrDefault(payerName, 0L) + pointsToDeduct);

            points = points - pointsToDeduct;

        }

        List<TransactionDto> payerDeductedPointsTransactions = buildPayerDeductedPointsTransactions(payerDeductedPoints);

        repo.saveDeductedPointsTransactions(userId, payerDeductedPointsTransactions);
        return payerDeductedPointsTransactions;
    }



    private List<TransactionDto> buildPayerDeductedPointsTransactions(Map<String, Long> payerDeductedPoints) {
        return payerDeductedPoints.entrySet().stream().map(e -> new TransactionDto(e.getKey(), -e.getValue(), LocalDateTime.now())).collect(Collectors.toList());
    }

    private void debitPayerAccount(long userId, String payerName, long points) {
        long newPayerPoints = repo.getTotalPayerPoints(userId, payerName) - points;
        repo.updatePayerTotalPoints(userId, payerName, newPayerPoints);
    }

    private void creditPayerAccount(long userId, String payerName, long points) {
        long newPayerPoints = repo.getTotalPayerPoints(userId, payerName) + points;
        repo.updatePayerTotalPoints(userId, payerName, newPayerPoints);
    }
}
