package com.fetchrewards.demopointsapi.exceptions;

import com.fetchrewards.demopointsapi.dto.ErrorDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class PointAccountControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler( value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request){
        return handleExceptionInternal(ex, new ErrorDto(ex.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler( value = {Exception.class})
    protected ResponseEntity<Object> handleGeneralException(Exception ex, WebRequest request){
        return handleExceptionInternal(ex, new ErrorDto(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler( value = {NotEnoughPointsToDeductException.class})
    protected ResponseEntity<Object> notEnoughPointsToDeduct(NotEnoughPointsToDeductException ex, WebRequest request){
        return handleExceptionInternal(ex, new ErrorDto(ex.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler( value = {NoUserIdException.class})
    protected ResponseEntity<Object> noUserId(NoUserIdException ex, WebRequest request){
        return handleExceptionInternal(ex, new ErrorDto(ex.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        StringBuilder sb = new StringBuilder();
        for (FieldError fErr : ex.getBindingResult().getFieldErrors()) {
            sb.append("property: ")
                    .append(fErr.getField())
                    .append(" ")
                    .append(fErr.getDefaultMessage())
                    .append(" ");
        }

        for (ObjectError oErr : ex.getBindingResult().getGlobalErrors()) {
            sb.append("objectName: ")
                    .append(oErr.getObjectName())
                    .append(" ")
                    .append(oErr.getDefaultMessage())
                    .append(" ");
        }

        return handleExceptionInternal(ex, new ErrorDto(sb.toString()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("API error", ex);
        if(!(body instanceof ErrorDto)){
            body = new ErrorDto(ex.getMessage());
        }

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }
}
