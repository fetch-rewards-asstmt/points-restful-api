package com.fetchrewards.demopointsapi.exceptions;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = NotZeroConstraintValidator.class)
public @interface NotZeroConstraint {
    String message() default "0 not allowed. Enter any number other than 0";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
