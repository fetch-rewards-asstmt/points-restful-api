package com.fetchrewards.demopointsapi.exceptions;

public class NoUserIdException extends RuntimeException {
    public NoUserIdException(long userId) {
        super("No user with id: "+userId);
    }
}
