package com.fetchrewards.demopointsapi.exceptions;

public class NotEnoughPointsToDeductException extends RuntimeException {
    public NotEnoughPointsToDeductException(){
        super("Not enough points to deduct");
    }
}

