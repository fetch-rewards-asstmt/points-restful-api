package com.fetchrewards.demopointsapi.exceptions;

public class NoTxRecordException extends RuntimeException {
    public NoTxRecordException(String message) {
        super(message);
    }
}
