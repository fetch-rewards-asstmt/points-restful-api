package com.fetchrewards.demopointsapi.exceptions;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotZeroConstraintValidator implements ConstraintValidator<NotZeroConstraint, Long> {
    @Override
    public boolean isValid(Long points, ConstraintValidatorContext constraintValidatorContext) {
        return points != 0;
    }
}
