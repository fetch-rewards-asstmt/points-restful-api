package com.fetchrewards.demopointsapi.dto;

import com.fetchrewards.demopointsapi.exceptions.NotZeroConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewPointsDto {
    @NotBlank
    private String payerName;

    @NotNull
    @NotZeroConstraint
    private long points;

    @NotNull
    @PastOrPresent
    private LocalDateTime transactionDate;

}
