package com.fetchrewards.demopointsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {
    private String payerName;
    private long points;
    private LocalDateTime creationDate;
}
