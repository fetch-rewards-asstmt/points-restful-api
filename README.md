## Demo Points API

API to add, deduct and view balance of user points. 

---

## How to test

The project runs on Java 8 or higher. It's a simple maven project.
Within the project root, run the following commands.

1.  ```./mvnw -DskipTests package```
2.  ```java -jar -Dserver.port=7070 ./target/demo-points-api-0.0.1-SNAPSHOT.jar```
    (You can change the value ```7070``` to any other valid port number)
3.  Open the link: [http://localhost:7070/api/swagger-ui/#/](http://localhost:7070/api/swagger-ui/#/point-account-controller)

The link above opens the auto-generate API documentation and testing UI. Please note that you can use any other rest client to test.

---

## Dependencies

1. JDK 8 and above